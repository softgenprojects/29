# Backend API Documentation

This document provides the details of the backend API endpoints for the frontend-handover process. It includes all the necessary information for frontend developers to integrate with the backend services effectively.

## Base URL

All API requests are made to the base URL: `https://(29-api.app.softgen.ai/)`

## Authentication

Some endpoints require authentication. The token must be provided in the `Authorization` header as `Bearer {token}`.

## User Endpoints

### Register User

- URL: `https://(29-api.app.softgen.ai/api/users/register)`
- Method: `POST`
- Authentication: No
- Request Body:
  ```json
  {
    "email": "string",
    "password": "string",
    "name": "string"
  }
  ```
- Success Response:
  - Code: 201
  - Content:
    ```json
    {
      "user": {
        "id": "number",
        "email": "string",
        "name": "string"
      },
      "token": "string"
    }
    ```
- Error Response:
  - Code: 400
  - Content: `{ "message": "string" }`

### Login User

- URL: `https://(29-api.app.softgen.ai/api/users/login)`
- Method: `POST`
- Authentication: No
- Request Body:
  ```json
  {
    "email": "string",
    "password": "string"
  }
  ```
- Success Response:
  - Code: 200
  - Content:
    ```json
    {
      "user": {
        "id": "number",
        "email": "string",
        "name": "string"
      },
      "token": "string"
    }
    ```
- Error Response:
  - Code: 401
  - Content: `{ "message": "string" }`

### Get User Projects

- URL: `https://(29-api.app.softgen.ai/api/users/projects)`
- Method: `GET`
- Authentication: Yes
- Success Response:
  - Code: 200
  - Content:
    ```json
    {
      "projects": [
        {
          "id": "number",
          "name": "string",
          "ownerId": "number",
          "createdAt": "date",
          "updatedAt": "date"
        }
      ]
    }
    ```
- Error Response:
  - Code: 404
  - Content: `{ "message": "string" }`

## Project Endpoints

### Create Project

- URL: `https://(29-api.app.softgen.ai/api/projects)`
- Method: `POST`
- Authentication: No
- Request Body:
  ```json
  {
    "name": "string",
    "ownerId": "number"
  }
  ```
- Success Response:
  - Code: 201
  - Content:
    ```json
    {
      "id": "number",
      "name": "string",
      "ownerId": "number",
      "createdAt": "date",
      "updatedAt": "date"
    }
    ```
- Error Response:
  - Code: 500
  - Content: `{ "message": "string" }`

### Get All Projects

- URL: `https://(29-api.app.softgen.ai/api/projects)`
- Method: `GET`
- Authentication: No
- Success Response:
  - Code: 200
  - Content:
    ```json
    [
      {
        "id": "number",
        "name": "string",
        "ownerId": "number",
        "createdAt": "date",
        "updatedAt": "date"
      }
    ]
    ```
- Error Response:
  - Code: 500
  - Content: `{ "message": "string" }`

### Get Project by ID

- URL: `https://(29-api.app.softgen.ai/api/projects/{id})`
- Method: `GET`
- Authentication: No
- URL Parameters: `id=[integer]`
- Success Response:
  - Code: 200
  - Content:
    ```json
    {
      "id": "number",
      "name": "string",
      "ownerId": "number",
      "createdAt": "date",
      "updatedAt": "date"
    }
    ```
- Error Response:
  - Code: 404
  - Content: `{ "message": "Project not found" }`

### Update Project

- URL: `https://(29-api.app.softgen.ai/api/projects/{id})`
- Method: `PUT`
- Authentication: No
- URL Parameters: `id=[integer]`
- Request Body:
  ```json
  {
    "name": "string"
  }
  ```
- Success Response:
  - Code: 200
  - Content:
    ```json
    {
      "id": "number",
      "name": "string",
      "ownerId": "number",
      "createdAt": "date",
      "updatedAt": "date"
    }
    ```
- Error Response:
  - Code: 500
  - Content: `{ "message": "string" }`

### Delete Project

- URL: `https://(29-api.app.softgen.ai/api/projects/{id})`
- Method: `DELETE`
- Authentication: No
- URL Parameters: `id=[integer]`
- Success Response:
  - Code: 204
  - Content: `No Content`
- Error Response:
  - Code: 500
  - Content: `{ "message": "string" }`

## Task Endpoints

### Create Task

- URL: `https://(29-api.app.softgen.ai/api/tasks)`
- Method: `POST`
- Authentication: No
- Request Body:
  ```json
  {
    "title": "string",
    "description": "string",
    "status": "string",
    "priority": "string",
    "projectId": "number"
  }
  ```
- Success Response:
  - Code: 201
  - Content:
    ```json
    {
      "id": "number",
      "title": "string",
      "description": "string",
      "status": "string",
      "priority": "string",
      "projectId": "number",
      "createdAt": "date",
      "updatedAt": "date"
    }
    ```
- Error Response:
  - Code: 400
  - Content: `{ "message": "string" }`

### Get All Tasks

- URL: `https://(29-api.app.softgen.ai/api/tasks)`
- Method: `GET`
- Authentication: No
- Success Response:
  - Code: 200
  - Content:
    ```json
    [
      {
        "id": "number",
        "title": "string",
        "description": "string",
        "status": "string",
        "priority": "string",
        "projectId": "number",
        "createdAt": "date",
        "updatedAt": "date"
      }
    ]
    ```
- Error Response:
  - Code: 400
  - Content: `{ "message": "string" }`

### Get Task by ID

- URL: `https://(29-api.app.softgen.ai/api/tasks/{id})`
- Method: `GET`
- Authentication: No
- URL Parameters: `id=[integer]`
- Success Response:
  - Code: 200
  - Content:
    ```json
    {
      "id": "number",
      "title": "string",
      "description": "string",
      "status": "string",
      "priority": "string",
      "projectId": "number",
      "createdAt": "date",
      "updatedAt": "date"
    }
    ```
- Error Response:
  - Code: 404
  - Content: `{ "message": "Task not found" }`

### Update Task

- URL: `https://(29-api.app.softgen.ai/api/tasks/{id})`
- Method: `PUT`
- Authentication: No
- URL Parameters: `id=[integer]`
- Request Body:
  ```json
  {
    "title": "string",
    "description": "string",
    "status": "string",
    "priority": "string"
  }
  ```
- Success Response:
  - Code: 200
  - Content:
    ```json
    {
      "id": "number",
      "title": "string",
      "description": "string",
      "status": "string",
      "priority": "string",
      "projectId": "number",
      "createdAt": "date",
      "updatedAt": "date"
    }
    ```
- Error Response:
  - Code: 400
  - Content: `{ "message": "string" }`

### Delete Task

- URL: `https://(29-api.app.softgen.ai/api/tasks/{id})`
- Method: `DELETE`
- Authentication: No
- URL Parameters: `id=[integer]`
- Success Response:
  - Code: 204
  - Content: `No Content`
- Error Response:
  - Code: 400
  - Content: `{ "message": "string" }`

## Error Codes

The following error codes are used in the API:

- `400` Bad Request
- `401` Unauthorized
- `403` Forbidden
- `404` Not Found
- `409` Conflict
- `500` Internal Server Error

## Examples

The following are example cURL requests for each endpoint:

(Insert cURL requests here, copied 1to1 from the latest cURL requests list)