const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const hashPassword = async (password) => {
  const salt = await bcrypt.genSalt(10);
  return bcrypt.hash(password, salt);
};

const verifyPassword = async (password, hashedPassword) => {
  return bcrypt.compare(password, hashedPassword);
};

const generateToken = (user) => {
  const jwtSecret = process.env.JWT_SECRET || 'secret';
  return jwt.sign({ id: user.id }, jwtSecret, { expiresIn: '1h' });
};

const verifyToken = (token) => {
  try {
    const jwtSecret = process.env.JWT_SECRET || 'secret';
    return jwt.verify(token, jwtSecret);
  } catch (error) {
    return null;
  }
};

module.exports = { hashPassword, verifyPassword, generateToken, verifyToken };
