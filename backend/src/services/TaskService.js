const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const { getProjectById } = require('@services/ProjectService.js');

const createTask = async (taskData) => {
  const project = await getProjectById(taskData.projectId);
  if (!project) {
    throw { status: 404, message: 'Project not found' };
  }
  return await prisma.task.create({
    data: taskData,
  });
};

const getTasks = async () => {
  return await prisma.task.findMany();
};

const getTaskById = async (id) => {
  return await prisma.task.findUnique({
    where: { id: parseInt(id) },
  });
};

const updateTask = async (id, taskData) => {
  return await prisma.task.update({
    where: { id: parseInt(id) },
    data: taskData,
  });
};

const deleteTask = async (id) => {
  return await prisma.task.delete({
    where: { id: parseInt(id) },
  });
};

module.exports = {
  createTask,
  getTasks,
  getTaskById,
  updateTask,
  deleteTask
};