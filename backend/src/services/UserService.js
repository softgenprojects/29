const { PrismaClient } = require('@prisma/client');
const { hashPassword, generateToken, verifyPassword } = require('@utils/AuthUtils');

const prisma = new PrismaClient();

const createUser = async (userData) => {
  const existingUser = await prisma.user.findUnique({
    where: { email: userData.email },
  });
  if (existingUser) {
    throw { status: 409, message: 'Email already in use' };
  }
  const hashedPassword = await hashPassword(userData.password);
  const user = await prisma.user.create({
    data: {
      ...userData,
      password: hashedPassword,
    },
  });
  const token = generateToken(user);
  return { user, token };
};

const authenticateUser = async (email, password) => {
  const user = await prisma.user.findUnique({
    where: { email },
  });
  if (!user) {
    throw new Error('User not found');
  }
  const isPasswordValid = await verifyPassword(password, user.password);
  if (!isPasswordValid) {
    throw new Error('Invalid password');
  }
  const token = generateToken(user);
  return { user, token };
};

const getUserProjects = async (userId) => {
  const userWithProjects = await prisma.user.findUnique({
    where: { id: userId },
    include: { projects: true },
  });
  return userWithProjects ? userWithProjects.projects : [];
};

module.exports = { createUser, authenticateUser, getUserProjects };
