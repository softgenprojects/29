const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

const createProject = async (projectData) => {
  const project = await prisma.project.create({
    data: projectData,
  });
  return project;
};

const getProjects = async () => {
  return await prisma.project.findMany();
};

const getProjectById = async (projectId) => {
  const project = await prisma.project.findUnique({
    where: { id: parseInt(projectId) },
  });
  return project;
};

const updateProject = async (projectId, projectData) => {
  return await prisma.project.update({
    where: { id: parseInt(projectId) },
    data: projectData,
  });
};

const deleteProject = async (projectId) => {
  return await prisma.project.delete({
    where: { id: parseInt(projectId) }
  });
};

module.exports = {
  createProject,
  getProjects,
  getProjectById,
  updateProject,
  deleteProject
};