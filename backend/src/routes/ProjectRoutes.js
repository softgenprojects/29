const express = require('express');
const router = express.Router();
const { createProjectController, getProjectsController, getProjectController, updateProjectController, deleteProjectController } = require('@controllers/ProjectController.js');

router.post('/', createProjectController);
router.get('/', getProjectsController);
router.get('/:id', getProjectController);
router.put('/:id', updateProjectController);
router.delete('/:id', deleteProjectController);

module.exports = router;