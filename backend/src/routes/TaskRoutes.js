const express = require('express');
const router = express.Router();
const { createTaskController, getTasksController, getTaskController, updateTaskController, deleteTaskController } = require('@controllers/TaskController.js');

router.post('/', createTaskController);
router.get('/', getTasksController);
router.get('/:id', getTaskController);
router.put('/:id', updateTaskController);
router.delete('/:id', deleteTaskController);

module.exports = router;