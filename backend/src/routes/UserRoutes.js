const express = require('express');
const { registerUser, loginUser, getUserProjectsController } = require('@controllers/UserController');
const { authenticate } = require('@middleware/AuthMiddleware');

const router = express.Router();

router.post('/register', registerUser);
router.post('/login', loginUser);
router.get('/projects', authenticate, getUserProjectsController);

module.exports = router;