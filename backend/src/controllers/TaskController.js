const { createTask, getTasks, getTaskById, updateTask, deleteTask } = require('@services/TaskService.js');

const createTaskController = async (req, res) => {
  try {
    const task = await createTask(req.body);
    res.status(201).json(task);
  } catch (error) {
    if (error.status) {
      res.status(error.status).json({ message: error.message });
    } else if (error.message === 'Project not found') {
      res.status(404).json({ message: error.message });
    } else {
      res.status(400).json({ message: 'An error occurred while creating the task' });
    }
  }
};

const getTasksController = async (req, res) => {
  try {
    const tasks = await getTasks();
    res.status(200).json(tasks);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getTaskController = async (req, res) => {
  try {
    const task = await getTaskById(req.params.id);
    if (task) {
      res.status(200).json(task);
    } else {
      res.status(404).json({ message: 'Task not found' });
    }
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const updateTaskController = async (req, res) => {
  try {
    const id = parseInt(req.params.id);
    const task = await updateTask(id, req.body);
    res.status(200).json(task);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const deleteTaskController = async (req, res) => {
  try {
    await deleteTask(req.params.id);
    res.status(204).send();
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

module.exports = {
  createTaskController,
  getTasksController,
  getTaskController,
  updateTaskController,
  deleteTaskController
};