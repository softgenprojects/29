const { createUser, authenticateUser, getUserProjects } = require('@services/UserService');

const registerUser = async (req, res) => {
  try {
    const { user, token } = await createUser(req.body);
    res.status(201).json({ user, token });
  } catch (error) {
    if (error.status === 409) {
      res.status(409).json({ message: 'Email already in use' });
    } else {
      res.status(400).json({ message: error.message });
    }
  }
};

const loginUser = async (req, res) => {
  try {
    const { email, password } = req.body;
    const { user, token } = await authenticateUser(email, password);
    res.status(200).json({ user, token });
  } catch (error) {
    res.status(401).json({ message: error.message });
  }
};

const getUserProjectsController = async (req, res) => {
  try {
    const userId = req.user.id;
    const projects = await getUserProjects(userId);
    res.status(200).json({ projects });
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

module.exports = { registerUser, loginUser, getUserProjectsController };
