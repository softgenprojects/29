const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

async function main() {
  // Create Users
  const admin = await prisma.user.create({
    data: {
      email: 'admin@example.com',
      password: 'hashed_password_admin',
      name: 'Admin'
    }
  });

  const user = await prisma.user.create({
    data: {
      email: 'user@example.com',
      password: 'hashed_password_user',
      name: 'User'
    }
  });

  // Create Projects
  const projectB = await prisma.project.create({
    data: {
      name: 'Project B',
      ownerId: admin.id
    }
  });

  // Assign Tasks to Users
  await prisma.task.createMany({
    data: [
      {
        title: 'Admin Task 1',
        description: 'Task for admin',
        status: 'OPEN',
        priority: 'HIGH',
        projectId: projectB.id,
        assigneeId: admin.id
      },
      {
        title: 'User Task 1',
        description: 'Task for user',
        status: 'IN_PROGRESS',
        priority: 'MEDIUM',
        projectId: projectB.id,
        assigneeId: user.id
      }
    ]
  });

  // Create more Users and Projects
  for (let i = 2; i <= 10; i++) {
    const newUser = await prisma.user.create({
      data: {
        email: `user${i}@example.com`,
        password: `hashed_password_user${i}`,
        name: `User ${i}`
      }
    });

    const newProject = await prisma.project.create({
      data: {
        name: `Project ${i}`,
        ownerId: newUser.id
      }
    });

    await prisma.task.create({
      data: {
        title: `Task ${i}`, 
        description: `Description for task ${i}`, 
        status: 'COMPLETED', 
        priority: 'LOW', 
        projectId: newProject.id, 
        assigneeId: newUser.id
      }
    });
  }

  // Log the created records
  const users = await prisma.user.findMany();
  const projects = await prisma.project.findMany();
  const tasks = await prisma.task.findMany();
  console.log({ users, projects, tasks });
}

main()
  .catch((e) => {
    throw e;
  })
  .finally(async () => {
    await prisma.$disconnect();
  });