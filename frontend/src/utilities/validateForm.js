export const validateForm = (formData) => {
  const results = {};

  // Required fields validation
  for (const [key, value] of Object.entries(formData)) {
    if (!value) {
      results[key] = 'This field is required';
    }
  }

  // Email format validation
  if (formData.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,7}$/i.test(formData.email)) {
    results.email = 'Invalid email address';
  }

  // Password strength validation
  if (formData.password && !/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/.test(formData.password)) {
    results.password = 'Password must be at least 8 characters long and include a number, a lowercase letter, and an uppercase letter';
  }

  return results;
};