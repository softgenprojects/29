import { format } from 'date-fns';

export const formatDate = (dateString, formatString) => {
  const date = new Date(dateString);
  return format(date, formatString);
};