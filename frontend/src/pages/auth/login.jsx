import React from 'react';
import { Box, Flex, Heading, useBreakpointValue, useToast } from '@chakra-ui/react';
import { LoginForm } from '@components/LoginForm';

const LoginPage = () => {
  const formContainerSize = useBreakpointValue({ base: '90%', md: '500px' });
  const toast = useToast();

  const handleServerError = (error) => {
    const description = error?.response?.data?.message || error.message || 'We are having some trouble. Please try again later.';
    toast({
      title: 'Server Error',
      description: description,
      status: 'error',
      duration: 9000,
      isClosable: true,
    });
  };

  return (
    <Flex
      minH='100vh'
      align='center'
      justify='center'
      bgGradient='linear(to-br, teal.400, purple.300)'
    >
      <Box
        p={8}
        boxShadow='xl'
        rounded='lg'
        bg='white'
        w={formContainerSize}
        mx='auto'
      >
        <Heading mb={6} textAlign='center'>Login</Heading>
        <LoginForm onError={handleServerError} />
      </Box>
    </Flex>
  );
};

export default LoginPage;