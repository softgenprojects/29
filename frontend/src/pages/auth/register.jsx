import React from 'react';
import { Center, Box, Heading, Text, VStack } from '@chakra-ui/react';
import { RegisterForm } from '@components/RegisterForm';

const RegisterPage = () => {
  return (
    <Center h='100vh' bgGradient='linear(to-br, teal.400, purple.500)'>
      <Box p={[4, 6, 8]} borderRadius='lg' boxShadow='xl' bg='white' w={['full', 'md']} maxW='lg'>
        <VStack spacing={8}>
          <Heading as='h1' size='xl' textAlign='center'>Create Account</Heading>
          <Text fontSize={{ base: 'md', md: 'lg' }} color='gray.600' textAlign='center'>
            Join us and manage your projects efficiently!
          </Text>
          <RegisterForm />
        </VStack>
      </Box>
    </Center>
  );
};

export default RegisterPage;
