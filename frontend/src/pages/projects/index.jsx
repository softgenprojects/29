import React from 'react';
import { Container, Heading, VStack, Alert, AlertIcon, AlertTitle, AlertDescription, CloseButton } from '@chakra-ui/react';
import { ProjectList } from '@components/ProjectList';
import { useProjectsList } from '@hooks/useProjects';
import * as ProjectsApi from '@api/ProjectsApi';

const ProjectsPage = () => {
  const { isError, error } = useProjectsList();

  if (isError) {
    return (
      <Container maxW='container.xl' py={{ base: '4', md: '8' }} centerContent>
        <Alert
          status='error'
          variant='subtle'
          flexDirection='column'
          alignItems='center'
          justifyContent='center'
          textAlign='center'
          height='200px'
        >
          <AlertIcon boxSize='40px' mr={0} />
          <AlertTitle mt={4} mb={1} fontSize='lg'>
            Error Loading Projects
          </AlertTitle>
          <AlertDescription maxWidth='sm'>
            {error.message || 'There was an issue fetching the projects. Please try again later.'}
          </AlertDescription>
          <CloseButton position='absolute' right='8px' top='8px' />
        </Alert>
      </Container>
    );
  }

  return (
    <Container maxW='container.xl' py={{ base: '4', md: '8' }}>
      <VStack spacing={{ base: '8', md: '10' }} align='stretch'>
        <Heading as='h1' size='xl' textAlign='center'>
          Project Management
        </Heading>
        <ProjectList />
      </VStack>
    </Container>
  );
};

export default ProjectsPage;
