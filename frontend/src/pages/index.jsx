import { Box, Stack, useColorModeValue, Center, Heading, Text, Button } from '@chakra-ui/react';
import { Hero } from '@components/Hero';
import { FeaturesSection } from '@components/FeaturesSection';
import { TestimonialsSection } from '@components/TestimonialsSection';
import { Footer } from '@components/Footer';
import { ErrorBoundary } from 'react-error-boundary';

function ErrorFallback({error, resetErrorBoundary}) {
  return (
    <Center role="alert" minH="100vh">
      <Stack>
        <Heading>Something went wrong:</Heading>
        <Text>{error.message}</Text>
        <Button onClick={resetErrorBoundary}>Try again</Button>
      </Stack>
    </Center>
  );
}

const Home = () => {
  const bgColor = useColorModeValue('gray.50', 'gray.900');
  const textColor = useColorModeValue('gray.800', 'whiteAlpha.900');

  return (
    <ErrorBoundary FallbackComponent={ErrorFallback}>
      <Box bg={bgColor} color={textColor} minH="100vh" py={{ base: '4', md: '8' }} px={{ base: '4', md: '8' }}>
        <Stack spacing={{ base: '8', md: '20' }}>
          <Hero />
          <FeaturesSection />
          <TestimonialsSection />
          <Footer />
        </Stack>
      </Box>
    </ErrorBoundary>
  );
};

export default Home;
