import React from 'react';
import { Container, Heading, VStack, Alert, AlertIcon, AlertTitle, AlertDescription } from '@chakra-ui/react';
import { TaskList } from '@components/TaskList';
import { useTasksList } from '@hooks/useTasks';

const TasksPage = () => {
  const { data: tasks, isLoading, isError, error } = useTasksList();

  if (isError) {
    return (
      <Container maxW='container.xl' p={5}>
        <VStack spacing={8} align='stretch'>
          <Alert status='error'>
            <AlertIcon />
            <AlertTitle mr={2}>Error!</AlertTitle>
            <AlertDescription>{error.message}</AlertDescription>
          </Alert>
        </VStack>
      </Container>
    );
  }

  return (
    <Container maxW='container.xl' p={5}>
      <VStack spacing={8} align='stretch'>
        <Heading as='h1' size='xl' textAlign='center'>Tasks</Heading>
        <TaskList tasks={tasks} isLoading={isLoading} />
      </VStack>
    </Container>
  );
};

export default TasksPage;