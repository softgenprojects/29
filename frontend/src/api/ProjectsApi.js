import axios from 'axios';

export const createProject = async (projectData) => {
  try {
    const response = await axios.post('https://29-api.app.softgen.ai/api/projects', projectData);
    return response.data;
  } catch (error) {
    if (error.response) {
      throw new Error(error.response.data.message || 'Error creating project');
    } else if (error.request) {
      throw new Error('The request was made but no response was received');
    } else {
      throw new Error('Something went wrong during the request');
    }
  }
};

export const getAllProjects = async () => {
  try {
    const response = await axios.get('https://29-api.app.softgen.ai/api/projects');
    return response.data;
  } catch (error) {
    if (error.response) {
      throw new Error(error.response.data.message || 'Error fetching all projects');
    } else if (error.request) {
      throw new Error('The request was made but no response was received');
    } else {
      throw new Error('Something went wrong during the request');
    }
  }
};

export const getProjectById = async (projectId) => {
  try {
    const response = await axios.get(`https://29-api.app.softgen.ai/api/projects/${projectId}`);
    return response.data;
  } catch (error) {
    if (error.response) {
      throw new Error(error.response.data.message || 'Error fetching project by ID');
    } else if (error.request) {
      throw new Error('The request was made but no response was received');
    } else {
      throw new Error('Something went wrong during the request');
    }
  }
};

export const updateProject = async (projectId, updateData) => {
  try {
    const response = await axios.put(`https://29-api.app.softgen.ai/api/projects/${projectId}`, updateData);
    return response.data;
  } catch (error) {
    if (error.response) {
      throw new Error(error.response.data.message || 'Error updating project');
    } else if (error.request) {
      throw new Error('The request was made but no response was received');
    } else {
      throw new Error('Something went wrong during the request');
    }
  }
};

export const deleteProject = async (projectId) => {
  try {
    const response = await axios.delete(`https://29-api.app.softgen.ai/api/projects/${projectId}`);
    return response.data;
  } catch (error) {
    if (error.response) {
      throw new Error(error.response.data.message || 'Error deleting project');
    } else if (error.request) {
      throw new Error('The request was made but no response was received');
    } else {
      throw new Error('Something went wrong during the request');
    }
  }
};