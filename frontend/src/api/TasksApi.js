import axios from 'axios';

export const createTask = async (taskData) => {
  try {
    const response = await axios.post('https://29-api.app.softgen.ai/api/tasks', taskData);
    return response.data;
  } catch (error) {
    if (error.response) {
      // Handle the response error
      throw new Error(error.response.data.message);
    } else if (error.request) {
      // The request was made but no response was received
      throw new Error('No response was received');
    } else {
      // Something happened in setting up the request that triggered an Error
      throw new Error('Error', error.message);
    }
  }
};

export const getAllTasks = async () => {
  try {
    const response = await axios.get('https://29-api.app.softgen.ai/api/tasks');
    return response.data;
  } catch (error) {
    // Handle errors
    throw error;
  }
};

export const getTaskById = async (taskId) => {
  try {
    const response = await axios.get(`https://29-api.app.softgen.ai/api/tasks/${taskId}`);
    return response.data;
  } catch (error) {
    // Handle errors
    throw error;
  }
};

export const updateTask = async (taskId, updateData) => {
  try {
    const response = await axios.put(`https://29-api.app.softgen.ai/api/tasks/${taskId}`, updateData);
    return response.data;
  } catch (error) {
    // Handle errors
    throw error;
  }
};

export const deleteTask = async (taskId) => {
  try {
    const response = await axios.delete(`https://29-api.app.softgen.ai/api/tasks/${taskId}`);
    return response.data;
  } catch (error) {
    // Handle errors
    throw error;
  }
};