import { useQuery, useMutation } from '@tanstack/react-query';
import { createProject, getAllProjects, getProjectById, updateProject, deleteProject } from '@api/ProjectsApi';

export const useCreateProject = () => {
  return useMutation({ mutationFn: createProject });
};

export const useProjectsList = () => {
  return useQuery({ queryKey: ['projects'], queryFn: getAllProjects, onError: (error) => {
    throw new Error(error.message || 'Error fetching projects');
  }});
};

export const useProject = (projectId) => {
  return useQuery({ queryKey: ['project', projectId], queryFn: () => getProjectById(projectId), onError: (error) => {
    throw new Error(error.message || 'Error fetching project by ID');
  }});
};

export const useUpdateProject = () => {
  return useMutation({ mutationFn: updateProject, onError: (error) => {
    throw new Error(error.message || 'Error updating project');
  }});
};

export const useDeleteProject = () => {
  return useMutation({ mutationFn: deleteProject, onError: (error) => {
    throw new Error(error.message || 'Error deleting project');
  }});
};