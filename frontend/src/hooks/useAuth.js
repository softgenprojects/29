import { useMutation } from '@tanstack/react-query';
import { registerUser, loginUser } from '@api/UsersApi';

export const useRegister = () => {
  return useMutation(registerUser, {
    onSuccess: (data) => {
      // Handle successful registration
    },
    onError: (error) => {
      // Handle registration error
    }
  });
};

export const useLogin = () => {
  return useMutation(loginUser, {
    onSuccess: (data) => {
      // Handle successful login
    },
    onError: (error) => {
      // Handle login error
    }
  });
};