import { useQuery, useMutation } from '@tanstack/react-query';
import { createTask, getAllTasks, getTaskById, updateTask, deleteTask } from '@api/TasksApi';

export const useCreateTask = () => {
  return useMutation(createTask);
};

export const useTasksList = () => {
  return useQuery({
    queryKey: ['tasks'],
    queryFn: getAllTasks
  });
};

export const useTask = (taskId) => {
  return useQuery({
    queryKey: ['task', taskId],
    queryFn: () => getTaskById(taskId)
  });
};

export const useUpdateTask = () => {
  return useMutation(updateTask);
};

export const useDeleteTask = () => {
  return useMutation(deleteTask);
};