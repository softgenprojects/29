import React from 'react';
import { Box, List, ListItem, ListIcon, Text, Spinner, Alert, AlertIcon, AlertTitle, AlertDescription, Center } from '@chakra-ui/react';
import { MdCheckCircle } from 'react-icons/md';
import { useTasksList } from '@hooks/useTasks';

export const TaskList = () => {
  const { data: tasks, isLoading, isError, error } = useTasksList();

  if (isLoading) {
    return (
      <Center>
        <Spinner />
      </Center>
    );
  }

  if (isError) {
    return (
      <Alert status='error'>
        <AlertIcon />
        <AlertTitle mr={2}>Error!</AlertTitle>
        <AlertDescription>{error.message}</AlertDescription>
      </Alert>
    );
  }

  return (
    <Box p={5} shadow='md' borderWidth='1px'>
      <Text fontSize='xl' mb={4}>Tasks</Text>
      <List spacing={3}>
        {tasks?.map((task) => (
          <ListItem key={task.id}>
            <ListIcon as={MdCheckCircle} color='green.500' />
            {task.title}
          </ListItem>
        ))}
      </List>
    </Box>
  );
};