import { Heading } from '@chakra-ui/react';

export const StyledHeading = ({ level, children, ...props }) => {
  const sizes = {
    h1: { base: '36px', md: '48px', lg: '64px' },
    h2: { base: '30px', md: '36px', lg: '48px' },
    h3: { base: '24px', md: '30px', lg: '36px' },
    h4: { base: '20px', md: '24px', lg: '30px' },
    h5: { base: '18px', md: '20px', lg: '24px' },
    h6: { base: '16px', md: '18px', lg: '20px' }
  };

  const fontWeight = 'semibold';
  const lineHeight = 'shorter';
  const letterSpacing = 'wide';

  return (
    <Heading
      as={level}
      fontSize={sizes[level]}
      fontWeight={fontWeight}
      lineHeight={lineHeight}
      letterSpacing={letterSpacing}
      {...props}
    >
      {children}
    </Heading>
  );
};