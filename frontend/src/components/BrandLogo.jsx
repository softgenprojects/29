import React from 'react';
import { Image } from '@chakra-ui/react';

export const BrandLogo = () => {
  const localLogo = 'https://picsum.photos/200'; // Updated path to an external logo image

  return (
    <Image src={localLogo} alt='Brand Logo' />
  );
};