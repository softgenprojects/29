import { Box, Link, Icon, Stack, useBreakpointValue, SimpleGrid } from '@chakra-ui/react';
import { FaFacebook, FaTwitter, FaInstagram } from 'react-icons/fa';
import { StyledText } from '@components/StyledText';

export const Footer = () => {
  const socialIcons = [
    { icon: FaFacebook, label: 'Facebook' },
    { icon: FaTwitter, label: 'Twitter' },
    { icon: FaInstagram, label: 'Instagram' }
  ];

  const isMobile = useBreakpointValue({ base: true, md: false });

  return (
    <Box
      as='footer'
      bg='blackAlpha.900'
      color='white'
      py={{ base: '12', md: '16' }}
      px={{ base: '6', md: '16' }}
    >
      <SimpleGrid
        columns={{ base: 1, md: 3 }}
        spacing={{ base: '8', md: '16' }}
      >
        <Box>
          <StyledText mb='4'>Navigation</StyledText>
          <Stack spacing='2'>
            <Link href='/about'><StyledText>About</StyledText></Link>
            <Link href='/contact'><StyledText>Contact</StyledText></Link>
            <Link href='/faq'><StyledText>FAQ</StyledText></Link>
          </Stack>
        </Box>
        <Box>
          <StyledText mb='4'>Contact</StyledText>
          <StyledText>Email: contact@example.com</StyledText>
          <StyledText>Phone: (123) 456-7890</StyledText>
        </Box>
        <Box>
          <StyledText mb='4'>Follow Us</StyledText>
          <Stack direction='row' spacing='4'>
            {socialIcons.map((social, index) => (
              <Link key={index} href={`https://${social.label.toLowerCase()}.com`} isExternal>
                <Icon as={social.icon} boxSize={{ base: '5', md: '6' }} />
              </Link>
            ))}
          </Stack>
        </Box>
      </SimpleGrid>
      <Box textAlign='center' mt={{ base: '12', md: '16' }}>
        <StyledText>© 2023 Company Name. All rights reserved.</StyledText>
      </Box>
    </Box>
  );
};