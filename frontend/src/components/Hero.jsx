import { Box } from '@chakra-ui/react';
import { StyledHeading, StyledButton } from '@components/StyledComponents/index';

export const Hero = () => {
  return (
    <Box
      display='flex'
      justifyContent='center'
      alignItems='center'
      p={{ base: 4, md: 8 }}
      bgImage='url(https://picsum.photos/1920/1080)'
      bgPos='center'
      bgSize='cover'
      color='white'
      textAlign='center'
      minHeight='100vh'
      width='full'
    >
      <Box>
        <StyledHeading
          as='h1'
          fontSize={{ base: '4xl', md: '6xl', lg: '8xl' }}
          fontWeight='bold'
          mb={6}
        >
          Simplify Your Workflow
        </StyledHeading>
        <StyledButton
          size={{ base: 'md', md: 'lg' }}
          colorScheme='whiteAlpha'
          variant='outline'
        >
          Discover More
        </StyledButton>
      </Box>
    </Box>
  );
};