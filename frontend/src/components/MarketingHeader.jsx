import { Flex, Box, Heading, Text, Button, Link, useBreakpointValue } from '@chakra-ui/react';

export const MarketingHeader = () => {
  const buttonSize = useBreakpointValue({ base: 'sm', md: 'md' });
  const bgGradient = useBreakpointValue({
    base: 'linear(to-r, teal.300, blue.500)',
    md: 'linear(to-l, teal.500, green.500)'
  });

  return (
    <Box as='header'>
      <Flex as='nav' justify='space-between' wrap='wrap' p={4}>
        <Link href='#home' p={2}>Home</Link>
        <Link href='#features' p={2}>Features</Link>
        <Link href='#pricing' p={2}>Pricing</Link>
        <Link href='#about' p={2}>About</Link>
      </Flex>
      <Flex
        direction='column'
        justify='center'
        align='center'
        h={{ base: 'calc(100vh - 64px)', md: '70vh' }}
        bgGradient={bgGradient}
        color='white'
        textAlign='center'
        p={4}
      >
        <Heading as='h1' size='2xl' mb={4}>
          Welcome to Our Service
        </Heading>
        <Text fontSize='xl' mb={8}>
          The best solution for your business needs.
        </Text>
        <Button size={buttonSize} colorScheme='blue'>
          Get Started
        </Button>
      </Flex>
    </Box>
  );
};
