import { Heading, Text } from '@chakra-ui/react';

export const StyledHeading = ({ children, ...props }) => (
  <Heading as='h1' size='xl' my={4} {...props}>
    {children}
  </Heading>
);

export const StyledText = ({ children, ...props }) => (
  <Text fontSize='lg' my={2} {...props}>
    {children}
  </Text>
);