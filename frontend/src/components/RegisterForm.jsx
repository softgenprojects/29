import React from 'react';
import { useForm } from 'react-hook-form';
import { Button, FormControl, FormLabel, FormErrorMessage, Input, VStack } from '@chakra-ui/react';
import { useRegister } from '@hooks/useAuth';

export const RegisterForm = () => {
  const { register, handleSubmit, formState: { errors } } = useForm();
  const { register: doRegister, isError, error } = useRegister();

  const onSubmit = ({ name, email, password }) => {
    doRegister({ name, email, password });
  };

  return (
    <VStack as='form' onSubmit={handleSubmit(onSubmit)} spacing={4}>
      <FormControl isInvalid={errors.name}>
        <FormLabel htmlFor='name'>Name</FormLabel>
        <Input id='name' type='text' {...register('name', { required: 'Name is required' })} />
        <FormErrorMessage>{errors.name && errors.name.message}</FormErrorMessage>
      </FormControl>

      <FormControl isInvalid={errors.email}>
        <FormLabel htmlFor='email'>Email</FormLabel>
        <Input id='email' type='email' {...register('email', {
          required: 'Email is required',
          pattern: {
            value: /^\S+@\S+\.\S+$/,
            message: 'Invalid email address'
          }
        })} />
        <FormErrorMessage>{errors.email && errors.email.message}</FormErrorMessage>
      </FormControl>

      <FormControl isInvalid={errors.password}>
        <FormLabel htmlFor='password'>Password</FormLabel>
        <Input id='password' type='password' {...register('password', { required: 'Password is required' })} />
        <FormErrorMessage>{errors.password && errors.password.message}</FormErrorMessage>
      </FormControl>

      {isError && <FormErrorMessage>{error.message}</FormErrorMessage>}

      <Button type='submit' colorScheme='blue'>Register</Button>
    </VStack>
  );
};
