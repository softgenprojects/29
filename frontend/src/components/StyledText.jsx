import { Text } from '@chakra-ui/react';

export const StyledText = ({ children, ...props }) => {
  return (
    <Text
      color='gray.700'
      fontSize={{ base: '16px', md: '18px', lg: '20px' }}
      lineHeight='1.6'
      fontWeight='normal'
      letterSpacing='tight'
      {...props}
    >
      {children}
    </Text>
  );
};
