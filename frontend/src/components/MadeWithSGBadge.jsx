import React from 'react';

export const MadeWithSGBadge = () => {
  return (
    <div>
      <span>Made with </span>
      <strong>SoftGen</strong>
    </div>
  );
};