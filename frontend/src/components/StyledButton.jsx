import React from 'react';
import { Button } from '@chakra-ui/react';

export const StyledButton = ({ variant, children, ...props }) => {
  const primaryStyles = {
    bg: 'black',
    color: 'white',
    _hover: { bg: 'gray.700' },
    _active: { bg: 'gray.800' },
    transition: 'background-color 0.3s ease-in-out'
  };

  const secondaryStyles = {
    bg: 'white',
    color: 'black',
    _hover: { bg: 'gray.100' },
    _active: { bg: 'gray.200' },
    transition: 'background-color 0.3s ease-in-out'
  };

  const styles = variant === 'primary' ? primaryStyles : secondaryStyles;

  return (
    <Button
      borderRadius='md'
      boxShadow='sm'
      _focus={{ boxShadow: 'none' }}
      {...styles}
      {...props}
    >
      {children}
    </Button>
  );
};
