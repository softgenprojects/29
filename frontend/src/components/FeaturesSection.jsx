import { SimpleGrid, Box, Icon } from '@chakra-ui/react';
import { MdCheckCircle } from 'react-icons/md';
import { StyledHeading, StyledText } from './StyledComponents';

export const FeaturesSection = () => {
  const features = [
    {
      title: 'Feature One',
      description: 'Description of feature one.',
      icon: MdCheckCircle,
    },
    {
      title: 'Feature Two',
      description: 'Description of feature two.',
      icon: MdCheckCircle,
    },
    // Add more features as needed
  ];

  return (
    <SimpleGrid columns={{ base: 1, md: 2, lg: 3 }} spacing={10}>
      {features.map((feature, index) => (
        <Box
          key={index}
          p={5}
          shadow='sm'
          borderWidth='1px'
          borderRadius='lg'
          bg='white'
          _hover={{ bg: 'gray.50' }}
          display='flex'
          flexDirection='column'
          alignItems='center'
          justifyContent='center'
          textAlign='center'
        >
          <Icon as={feature.icon} w={10} h={10} color='green.500' mb={4} />
          <StyledHeading>{feature.title}</StyledHeading>
          <StyledText>{feature.description}</StyledText>
        </Box>
      ))}
    </SimpleGrid>
  );
};