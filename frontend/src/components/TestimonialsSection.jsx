import { Box, Text, useBreakpointValue, keyframes, usePrefersReducedMotion } from '@chakra-ui/react';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

export const TestimonialsSection = () => {
  const carouselSettings = {
    showArrows: false,
    infiniteLoop: true,
    showStatus: false,
    showIndicators: true,
    useKeyboardArrows: true,
    autoPlay: true,
    stopOnHover: true,
    swipeable: true,
    dynamicHeight: true,
    emulateTouch: true,
    showThumbs: false,
    interval: 5000,
    transitionTime: 1000,
  };

  const testimonials = [
    { quote: 'This product has changed my life for the better!', name: 'Jane Doe' },
    { quote: 'Absolutely fantastic! Can\'t recommend it enough.', name: 'John Smith' },
    { quote: 'The best service I have ever used.', name: 'Emma Jones' }
  ];

  const responsiveFontSize = useBreakpointValue({ base: 'sm', md: 'md', lg: 'lg' });
  const prefersReducedMotion = usePrefersReducedMotion();

  const slideIn = keyframes`
    from { opacity: 0; transform: translateX(-30px); }
    to { opacity: 1; transform: translateX(0); }
  `;

  const animation = prefersReducedMotion
    ? undefined
    : `${slideIn} 1s ease-out`;

  return (
    <Box py={{ base: 8, md: 10 }} px={{ base: 4, md: 6 }} bgGradient='linear(to-r, gray.100, gray.300)'>
      <Carousel {...carouselSettings}>
        {testimonials.map((testimonial, index) => (
          <Box key={index} p={5} m={2} bg='white' borderRadius='lg' boxShadow='2xl' animation={animation}>
            <Text fontSize={responsiveFontSize} fontStyle='italic' color='gray.600'>\"{testimonial.quote}\"</Text>
            <Text fontSize={responsiveFontSize} fontWeight='bold' mt={2} color='gray.800'>{testimonial.name}</Text>
          </Box>
        ))}
      </Carousel>
    </Box>
  );
};