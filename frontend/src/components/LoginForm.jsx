import React from 'react';
import { useForm } from 'react-hook-form';
import { Button, FormControl, FormLabel, Input, FormErrorMessage, Box, useToast } from '@chakra-ui/react';
import { useLogin } from '@hooks/useAuth';
import { useRouter } from 'next/router';

export const LoginForm = () => {
  const { register, handleSubmit, formState: { errors } } = useForm();
  const router = useRouter();
  const toast = useToast();
  const { mutate, isLoading, isError, error } = useLogin();

  const onSubmit = (values) => {
    mutate(values, {
      onSuccess: () => {
        router.push('/dashboard');
      },
      onError: (error) => {
        toast({
          title: 'Error logging in.',
          description: error.response?.data?.message || error.message,
          status: 'error',
          duration: 9000,
          isClosable: true,
        });
      }
    });
  };

  return (
    <Box p={4} maxWidth='500px' mx='auto'>
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormControl isInvalid={errors.email} mb={4}>
          <FormLabel htmlFor='email'>Email</FormLabel>
          <Input
            id='email'
            type='email'
            {...register('email', {
              required: 'This is required',
              pattern: {
                value: /^\S+@\S+\.\S+$/,
                message: 'Invalid email address'
              }
            })}
          />
          <FormErrorMessage>
            {errors.email && errors.email.message}
          </FormErrorMessage>
        </FormControl>
        <FormControl isInvalid={errors.password} mb={4}>
          <FormLabel htmlFor='password'>Password</FormLabel>
          <Input
            id='password'
            type='password'
            {...register('password', {
              required: 'This is required',
              minLength: {
                value: 6,
                message: 'Minimum length should be 6'
              }
            })}
          />
          <FormErrorMessage>
            {errors.password && errors.password.message}
          </FormErrorMessage>
        </FormControl>
        {isError && <Box color='red.500' mb={4}>{error?.response?.data?.message || error.message}</Box>}
        <Button mt={4} width='full' colorScheme='blue' isLoading={isLoading} type='submit' isDisabled={isLoading}>
          Login
        </Button>
      </form>
    </Box>
  );
};