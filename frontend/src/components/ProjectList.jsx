import React from 'react';
import { Box, List, ListItem, ListIcon, Heading, Text, Spinner, Alert, AlertIcon, Center } from '@chakra-ui/react';
import { MdCheckCircle } from 'react-icons/md';
import { useProjectsList } from '@hooks/useProjects';

export const ProjectList = () => {
  const { data: projects, isLoading, isError, error } = useProjectsList();

  if (isLoading) {
    return (
      <Center>
        <Spinner />
      </Center>
    );
  }

  if (isError) {
    return (
      <Alert status='error'>
        <AlertIcon />
        <Text>{error.message}</Text>
      </Alert>
    );
  }

  return (
    <Box p={5} shadow='md' borderWidth='1px'>
      <Heading mb={4}>Projects</Heading>
      <List spacing={3}>
        {projects?.map((project) => (
          <ListItem key={project.id}>
            <ListIcon as={MdCheckCircle} color='green.500' />
            {project.name}
          </ListItem>
        ))}
      </List>
    </Box>
  );
};